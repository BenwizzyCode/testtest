*** Settings ***
Documentation  Testing for web browser
Library  SeleniumLibrary


*** Variables ***
${browser}  Chrome
${url}  https://cvr.inecnigeria.org/pu


*** Test Cases ***
Filling INEC FORM     # user defined keyword
  Open Browser  ${url}  ${browser}
  Maximize Browser Window
  Page Should Contain  Polling Unit Locator

  # Selection Of State
  Page Should Contain List  xpath://*[@id="SearchStateId"]
  Page Should Not Contain List  xpath://*[@id="SearchStateIdd"]
  @{AllItems}=  Get List Items  xpath://*[@id="SearchStateId"]
  ${ListValue}=  Get Selected List Value  xpath://*[@id="SearchStateId"]
  List Selection Should Be  xpath://*[@id="SearchStateId"]  SELECT STATE
  Select From List By Value  xpath://*[@id="SearchStateId"]  29        #Selecting the state
  Sleep  2s
  ${LLabel1}  Get Selected List Label  xpath://*[@id="SearchStateId"]

  # Selection Of Local Government
  Page Should Contain List  xpath://*[@id="SearchLocalGovernmentId"]
  Page Should Not Contain List  xpath://*[@id="SearchLocalGovernmentIdd"]
  @{AllItems}=  Get List Items  xpath://*[@id="SearchLocalGovernmentId"]
  ${ListValue}=  Get Selected List Value  xpath://*[@id="SearchLocalGovernmentId"]
  List Selection Should Be  xpath://*[@id="SearchLocalGovernmentId"]  --SELECT--
  Select From List By Value  xpath://*[@id="SearchLocalGovernmentId"]  590       #Selecting the Local Government
  Sleep  2s
  ${LLabel2}  Get Selected List Label  xpath://*[@id="SearchLocalGovernmentId"]

  # Selection Of Registration Area
  Page Should Contain List  xpath://*[@id="SearchRegistrationAreaId"]
  Page Should Not Contain List  xpath://*[@id="SearchRegistrationAreaIdd"]
  @{AllItems}=  Get List Items  xpath://*[@id="SearchRegistrationAreaId"]
  ${ListValue}=  Get Selected List Value  xpath://*[@id="SearchRegistrationAreaId"]
  List Selection Should Be  xpath://*[@id="SearchRegistrationAreaId"]  --SELECT--
  Select From List By Value  xpath://*[@id="SearchRegistrationAreaId"]  6754     #Selecting the Registration Area
  Sleep  2s
  ${LLabel2}  Get Selected List Label  xpath://*[@id="SearchRegistrationAreaId"]

  # Selection Of Polling Unit
  Page Should Contain List  xpath://*[@id="SearchPollingUnitId"]
  Page Should Not Contain List  xpath://*[@id="SearchPollingUnitIdd"]
  @{AllItems}=  Get List Items  xpath://*[@id="SearchPollingUnitId"]
  ${ListValue}=  Get Selected List Value  xpath://*[@id="SearchPollingUnitId"]
  List Selection Should Be  xpath://*[@id="SearchPollingUnitId"]  --SELECT--
  Select From List By Value  xpath://*[@id="SearchPollingUnitId"]  94123     #Selecting the Polling Unit
  Sleep  2s
  ${LLabel2}  Get Selected List Label  xpath://*[@id="SearchPollingUnitId"]

  Sleep  10s
  Click Element  xpath://*[@id="SearchIndexForm"]/div[6]/input           #Selecting get Directions
  Close Browser
